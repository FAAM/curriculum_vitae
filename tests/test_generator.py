import os
import pytest
from cv import latex_generator


t1 = r"""\documentclass[12pt, a4paper]{article}
\begin{document}
    \VAR{Name}
    \VAR{LastName}
\end{document}"""

e1 = r"""\documentclass[12pt, a4paper]{article}
\begin{document}
    Sebastian
    Yonekura Baeza
\end{document}"""


@pytest.mark.parametrize("data, template, expected", [
    (
        dict(Name='Sebastian', LastName='Yonekura Baeza'), t1, e1
    )
])
def test_render_template(data, template, expected, tmp_path):
    template_file = tmp_path / 'template.tex'
    template_file.write_text(template)
    path = os.path.abspath(template_file)
    result = latex_generator.render_template(path, data)
    assert result == expected


@pytest.mark.parametrize("document, path", [
    ('some string', 'carpeta/algo.tex'),
    ('any', 'otro.tex')
])
def test_to_file(document, path, tmp_path):
    file = os.path.join(tmp_path, path)
    latex_generator.to_file(document, file)
    f = tmp_path / path
    assert f.read_text() == document
