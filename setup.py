from setuptools import setup


setup(
    name='curriculum_vitae',
    author='Francisco Alfaro Medina',
    install_requires=[
        'Click',
        'pyyaml'
    ],
    entry_points='''
    [console_scripts]
    curriculum_vitae=cv.latex_generator:make_templates
    '''
)
